package warmup_2;

public class Tasks {

    /**
     * @Task_Name : doubleX
     * @Description : Given a string, return true if the first instance of "x" in the string is immediately followed by another "x".
     * @Solved_By : Abdulaziz Hikmatov
     */
    public boolean doubleX(String str){
        int i = str.indexOf('x');
        if (i == -1) return false;
        if (i+1 >= str.length()) return false;
        return str.charAt(i + 1) == 'x';
    }

    /**
     * @Task_Name : stringBits
     * @Description : Given a string, return a new string made of every other char starting with the first, so "Hello" yields "Hlo".
     * @Solved_By : Abdulaziz Hikmatov
     */
    public String stringBits(String str){
        String result = "";
        for (int i = 0; i < str.length(); i+=2){
            result = result.concat(str.substring(i,i+1));
        }
        return result;
    }

    /**
     * @Task_Name : stringSplosion
     * @Description : Given a non-empty string like "Code" return a string like "CCoCodCode".
     * @Solved_By : Abdulaziz Hikmatov
     */
    public String stringSplosion(String str) {
        String str2 = "";
        for (int i = 1; i < str.length(); i++){
            str2 = str.substring(0, str.length() - i) + str;
        }
        return str;
    }
    /**
     * @Task_Name - theEnd
     * @Description - Given a string, return a string length 1 from its front, unless front is false, in which case return a string length 1 from its back. The string will be non-empty.
     * theEnd("Hello", true) → "H"
     * theEnd("Hello", false) → "o"
     * theEnd("oh", true) → "o"
     *
     * @Solved_By - Abdulaziz Hikmatov
     */
    public String theEnd(String str, boolean front) {
        return  front ? String.valueOf(str.charAt(0)) : String.valueOf(str.charAt(str.length() -1));
    }

    /**
     * @Task_Name - withouEnd2
     * @Description - Given a string, return a version without both the first and last char of the string. The string may be any length, including 0.
     * @Solved_By - Abdulaziz Hikmatov
     */
    public String withoutEnd2(String str) {
        String result = str;
        if (str.isEmpty()) return result;
        if (str.length() >= 2) {
            result = str.substring(1, str.length() - 1);
        }
        if (str.length() == 1){
            result = "";
        }
        return result;
    }

    public boolean hasBad(String str) {
        boolean result  = false;
        if(str.length() >= 3){
            if(str.startsWith("bad") || str.startsWith("bad", 1)){
                result = true;
            }
        }
        return result;
    }

}
